file(GLOB_RECURSE demo_blocksize_grayscale_SOURCES
        ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/*.h
        ${CMAKE_CURRENT_SOURCE_DIR}/*.cu
        ${CMAKE_CURRENT_SOURCE_DIR}/*.cuh
        )
cuda_add_executable(demo_blocksize_grayscale ${demo_blocksize_grayscale_SOURCES})

target_link_libraries(demo_blocksize_grayscale cuda_samples_common cudasoil)

install(TARGETS demo_blocksize_grayscale RUNTIME DESTINATION ${CMAKE_INSTALL_PREFIX})
