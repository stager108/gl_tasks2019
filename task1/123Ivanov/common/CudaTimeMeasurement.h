#pragma once

#include <driver_types.h>

class CudaTimeMeasurement final {
public:
	~CudaTimeMeasurement();

	void start();
	float stop();

	float getSeconds();
private:
	cudaEvent_t startEvent = nullptr;
	cudaEvent_t stopEvent = nullptr;

	enum Status {
		Uninitialized,
		Started,
		Finished,
		Invalid
	};
	Status status = Uninitialized;

	float lastTimeSeconds = 0;
};
